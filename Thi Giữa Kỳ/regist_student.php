
<html>
<head>
    <title>Thông tin đăng ký sinh viên</title>
</head>
<body>
    <h1>Thông tin đăng ký sinh viên</h1>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = $_POST["name"];
        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $otherInfo = $_POST["otherInfo"];
        
        echo "<p><strong>Họ và tên:</strong> $name</p>";
        echo "<p><strong>Giới tính:</strong> $gender</p>";
        echo "<p><strong>Ngày sinh:</strong> $birthday</p>";
        echo "<p><strong>Địa chỉ:</strong> $district - $city</p>";
        echo "<p><strong>Thông tin khác:</strong> $otherInfo</p>";
    } else {
        echo "<p>Không có thông tin đăng ký nào được gửi.</p>";
    }
    ?>
</body>
</html>
