<html>
<head>
    <title>Form đăng ký sinh viên</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #3498DB;
            color: white;
        }
        td {
            background-color: white;
        }
        td.left-column {
            background-color: #3498DB;
            color: white;
        }
        .center {
            text-align: center;
        }
        .button-container {
            text-align: center;
            margin-top: 20px;
        }
        #submitButton {
            padding: 10px 20px;
            font-size: 16px;
        }
        /* Add styles for form elements */
        input[type="text"],
        select,
        textarea {
            width: 100%;
            padding: 8px;
            margin: 5px 0;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        input[type="radio"] {
            margin-right: 5px;
        }
        /* Add styles for labels */
        label {
            display: block;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <h1>Form đăng ký sinh viên</h1>
    <form id="studentForm" action="regist_student.php" method="post" onsubmit="return validateForm()">
        <table>
            <tr>
                <td class="left-column">Họ và tên:</td>
                <td><input type="text" id="name" name="name" required></td>
            </tr>
            <tr>
                <td class="left-column">Giới tính:</td>
                <td>
                    <input type="radio" id="male" name="gender" value="Nam" required> Nam
                    <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
                </td>
            </tr>
            <tr>
                <td class="left-column">Ngày sinh:</td>
                <td>
                    <select id="year" name="year" required onchange="updateMonthsAndDays()">
                        <option value="" disabled selected>Chọn năm</option>
                        <?php
                        for ($year = 1940; $year <= 2015; $year++) {
                            echo "<option value='$year'>$year</option>";
                        }
                        ?>
                    </select>
                    <select id="month" name="month" required>
                        <option value="" disabled selected>Chọn tháng</option>
                    </select>
                    <select id="day" name="day" required>
                        <option value="" disabled selected>Chọn ngày</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="left-column">Địa chỉ:</td>
                <td>
                    <label for="city"> Thành Phố:</label>
                    <select id="city" name="city" required>
                        <option value="" disabled selected>Chọn thành phố</option>
                        <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                        <option value="Hà Nội">Hà Nội</option>
                    </select>
                    <label for="district"> Quận:</label>
                    <select id="district" name="district" required>
                        <option value="" disabled selected>Chọn quận</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="left-column">Thông tin khác:</td>
                <td><textarea id="otherInfo" name="otherInfo"></textarea></td>
            </tr>
        </table>
        <div class="button-container">
            <input type="submit" id="submitButton" value="Đăng ký">
        </div>
    </form>

    <script>
        function validateForm() {
            var name = document.getElementById("name").value;
            var gender = document.querySelector("input[name='gender']:checked");
            var year = document.getElementById("year").value;
            var month = document.getElementById("month").value;
            var day = document.getElementById("day").value;
            var city = document.getElementById("city").value;
            var district = document.getElementById("district").value;
            var errorMessages = [];

            if (!name) {
                errorMessages.push("Hãy nhập họ tên.");
            }

            if (!gender) {
                errorMessages.push("Hãy chọn giới tính.");
            }

            if (!year || !month || !day) {
                errorMessages.push("Hãy chọn ngày sinh đầy đủ.");
            }

            if (!city) {
                errorMessages.push("Hãy chọn địa chỉ.");
            }

            if (!district) {
                errorMessages.push("Hãy chọn quận.");
            }

            if (errorMessages.length > 0) {
                alert("Lỗi:\n" + errorMessages.join("\n"));
                return false;
            }

            // Xử lý đăng ký sinh viên ở đây

            return true;
        }

        document.getElementById("city").addEventListener("change", function() {
            var city = this.value;
            var districtSelect = document.getElementById("district");
            while (districtSelect.firstChild) {
                districtSelect.removeChild(districtSelect.firstChild);
            }
            if (city === "Hồ Chí Minh") {
                var districts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
            } else if (city === "Hà Nội") {
                var districts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
            }
            districts.forEach(function(district) {
                var option = document.createElement("option");
                option.value = district;
                option.text = district;
                districtSelect.appendChild(option);
            });
        });

        function updateMonthsAndDays() {
            var yearSelect = document.getElementById("year");
            var monthSelect = document.getElementById("month");
            var daySelect = document.getElementById("day");

            // Xóa tất cả các tùy chọn cũ trong select tháng và ngày
            while (monthSelect.firstChild) {
                monthSelect.removeChild(monthSelect.firstChild);
            }

            while (daySelect.firstChild) {
                daySelect.removeChild(daySelect.firstChild);
            }

            // Lấy năm đã chọn
            var selectedYear = parseInt(yearSelect.value);

            // Thêm tùy chọn cho select tháng
            for (var month = 1; month <= 12; month++) {
                var option = document.createElement("option");
                option.value = month;
                                option.text = month;
                monthSelect.appendChild(option);
            }

            // Thêm tùy chọn cho select ngày dựa trên tháng và năm đã chọn
            var selectedMonth = parseInt(monthSelect.value);
            var maxDays = new Date(selectedYear, selectedMonth, 0).getDate();
            for (var day = 1; day <= maxDays; day++) {
                var option = document.createElement("option");
                option.value = day;
                option.text = day;
                daySelect.appendChild(option);
            }
        }
    </script>
</body>
</html>

               
