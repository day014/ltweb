<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        .search-container {
            margin-top: 20px;
            display: flex;
            align-items: center;
        }

        .search-input,
        .add-button button {
            background-color: #0077cc;
            color: #fff;
            border: none;
            padding: 8px;
            cursor: pointer;
        }

        .search-input {
            width: 150px;
            margin-right: 8px;
        }
    </style>
</head>
<body>
    <h2>Danh sách sinh viên</h2>

    <div class="search-container">
        <label for="department">Khoa:</label>
        <select id="department">
            <option value="">Tất cả</option>
            <option value="khoa1">Khoa học máy tính</option>
            <option value="khoa2">Khoa học dữ liệu</option>
        </select>

        <label for="keyword">Từ khóa:</label>
        <input type="text" id="keyword" class="search-input">

        <button onclick="resetSearch()" class="search-input">Reset</button>
        <button onclick="searchStudents()" class="search-input">Tìm kiếm</button>

        <div id="result-message"></div>
    </div>

    <div class="add-button">
        <button onclick="openAddStudentForm()">Thêm</button>
    </div>

    <table id="student-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Tên Sinh Viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="student-table-body">
            <tr>
                <td>1</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <button onclick="editStudent(1)">Sửa</button>
                    <button onclick="confirmDelete(1)">Xóa</button>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Trần Thị B</td>
                <td>Khoa học dữ liệu</td>
                <td>
                    <button onclick="editStudent(2)">Sửa</button>
                    <button onclick="confirmDelete(2)">Xóa</button>
                </td>
            </tr>
            <!-- ... (additional rows) ... -->
        </tbody>
    </table>

    <script>
        function searchStudents() {
            var department = document.getElementById("department").value;
            var keyword = document.getElementById("keyword").value;
            console.log("AJAX call - Search with Department: " + department + ", Keyword: " + keyword);

            var resultMessage = document.getElementById("result-message");
            var numberOfStudents = 10;
            resultMessage.innerHTML = "Số sinh viên tìm thấy: " + numberOfStudents;
        }

        function resetSearch() {
            document.getElementById("department").value = "";
            document.getElementById("keyword").value = "";
        }

        function openAddStudentForm() {
            // Logic to open the form for adding a new student
        }

        function editStudent(studentId) {
            window.location.href = "update_students.php?id=" + studentId;
        }

        function confirmDelete(studentId) {
            var result = confirm("Bạn muốn xóa sinh viên này?");
            if (result) {
                deleteStudent(studentId);
            }
        }

        function deleteStudent(studentId) {
            // Logic to delete a student with the given studentId
            // Simulate AJAX call - replace with actual AJAX logic
            console.log("AJAX call - Delete Student with ID: " + studentId);
            
            // After deleting the student, you may want to reload the table or update the UI accordingly
        }
    </script>
</body>
</html>
