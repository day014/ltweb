<?php
// Thêm mã xử lý để lấy thông tin sinh viên dựa trên ID từ cơ sở dữ liệu

// Kết nối đến cơ sở dữ liệu (thay đổi thông tin kết nối của bạn)
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "your_database";

$conn = new mysqli($servername, $username, $password, $dbname);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Lấy thông tin sinh viên từ cơ sở dữ liệu dựa trên ID
if (isset($_GET['id'])) {
    $studentId = $_GET['id'];
    $sql = "SELECT * FROM students WHERE id = $studentId";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $studentName = $row['student_name'];
        $department = $row['department'];
        // Thêm các trường thông tin sinh viên khác cần sửa
    } else {
        echo "Không tìm thấy sinh viên có ID: " . $studentId;
        exit();
    }
} else {
    echo "ID sinh viên không được cung cấp";
    exit();
}

// Đóng kết nối đến cơ sở dữ liệu
$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cập nhật thông tin sinh viên</title>
    <!-- Thêm các thẻ style và các tệp CSS cần thiết cho trang cập nhật -->
</head>
<body>
    <h2>Cập nhật thông tin sinh viên</h2>

    <!-- Thêm các trường thông tin sinh viên cần sửa -->
    <form action="update_process.php" method="post">
        <input type="hidden" name="student_id" value="<?php echo $studentId; ?>">
        <label for="student_name">Tên Sinh Viên:</label>
        <input type="text" id="student_name" name="student_name" value="<?php echo $studentName; ?>" required>

        <label for="department">Khoa:</label>
        <select id="department" name="department">
            <option value="khoa1" <?php if ($department == 'khoa1') echo 'selected'; ?>>Khoa học máy tính</option>
            <option value="khoa2" <?php if ($department == 'khoa2') echo 'selected'; ?>>Khoa học dữ liệu</option>
        </select>

        <!-- Thêm các trường thông tin sinh viên khác cần sửa -->

        <button type="submit">Xác nhận</button>
    </form>
</body>
</html>
